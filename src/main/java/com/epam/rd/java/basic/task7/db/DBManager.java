package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	Connection connection;
	public static synchronized DBManager getInstance() {
		if (instance == null) {
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream("app.properties"));
				DBManager manager = new DBManager();
				//manager.setConnection(DriverManager.getConnection("jdbc:mysql://localhost:/test2db", "root", "5805"));
				//System.out.println(" all ok");
				manager.setConnection(DriverManager.getConnection(prop.getProperty("connection.url")));
				instance = manager;
			} catch (IOException | SQLException e) {
				throw new IllegalArgumentException("something wrong with db");
			}
		}
		return instance;
	}

	private void setConnection(Connection connection) {
		this.connection = connection;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM users"))
		{
			while (rs.next())
			{
				int id = rs.getInt("id");
				String login = rs.getString("login");
				//user.setId(rs.getInt("id"));
				//User user = new User(id, login);
				users.add(new User(id, login));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("findAllUsers(): failed to get list of users", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null)
		{
			return false;
		}

		try (Statement st = connection.createStatement();)
		{
			if (1 == st.executeUpdate("INSERT INTO users (login) VALUES ('" + user.getLogin() + "')", Statement.RETURN_GENERATED_KEYS))
			{
				try (ResultSet rs = st.getGeneratedKeys()){
					rs.next();
					user.setId(rs.getInt(1));
					return true;
				}
			}


		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("insertUser() failed", e);
		}
		return false;
	}


	public boolean deleteUsers(User... users) throws DBException {
		for (User user : users) {
			deleteUser(user);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM users WHERE login = '" + login + "'");
			if (resultSet.next()) {
				return new User(resultSet.getInt("id"), resultSet.getString("login"));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM teams WHERE name = '" + name + "'");
			if (resultSet.next()) {
				return new Team(resultSet.getInt("id"), resultSet.getString("name"));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
			while (resultSet.next()){
				teams.add(new Team(resultSet.getString("name")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null)
		{
			return false;
		}
		try (Statement st = connection.createStatement()){
			if (1 == st.executeUpdate("INSERT INTO teams (name) VALUES ('" + team.getName() +  "')", Statement.RETURN_GENERATED_KEYS)){
				try(ResultSet rs = st.getGeneratedKeys()) {
					rs.next();
					team.setId(rs.getInt(1));
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("getTeam() failed", e);
		}
		return false;

	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		User userInDb = getUser(user.getLogin());
		Map<Team, User> userTeamMap = new HashMap<>();
		for (Team team: teams){
			Team teamInDb = getTeam(team.getName());
			userTeamMap.put(teamInDb, userInDb);
		}

		Map<Team, User> userTeamMapSavedInDB = new HashMap<>();
		boolean isFailed = false;
		for(Map.Entry<Team, User> entry : userTeamMap.entrySet()) {
			String query = "INSERT INTO users_teams VALUES (" + entry.getValue().getId() + ", " + entry.getKey().getId() + ")";
			try (Statement stmt = connection.createStatement()) {
				stmt.executeUpdate(query);
			} catch (SQLException throwable) {
				isFailed = true;
				break;
			}
			userTeamMapSavedInDB.put(entry.getKey(), entry.getValue());
		}

		if (isFailed) {
			for(Map.Entry<Team, User> entry : userTeamMapSavedInDB.entrySet()) {
				try {
					deleteUserTeam(entry.getValue(), entry.getKey());

				} catch (Exception throwable){

				}
			}
			throw new DBException("Can not delete", new SQLException());
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();

		try (ResultSet rs = connection.createStatement().executeQuery("SELECT team_id, name FROM users_teams JOIN teams ON team_id = id WHERE user_id = " + user.getId())){

			while (rs.next())
			{
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("team_id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("getUserTeams() failed", e);
		}
		return teams;
	}



	public boolean deleteTeam(Team team) throws DBException {
		String query = "DELETE FROM teams WHERE name = '" + team.getName() + "'";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwables) {
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "UPDATE teams SET name = '" + team.getName() + "' WHERE id =" + team.getId();
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwables) {
			throw new DBException("Can not update", throwables);
		}
		return true;
	}

	public boolean deleteUserTeam(User user, Team team) throws DBException {
		String query = "DELETE FROM users_teams WHERE user_id = " + user.getId() + " AND team_id = " + team.getId() ;
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwable) {
			throw new DBException("Can not delete", throwable);
		}
		return true;
	}
	public boolean deleteUser(User user) throws DBException {
		String query = "DELETE FROM users WHERE login = '" + user.getLogin() + "'";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwables) {
			throw new DBException("Can not delete", throwables);
		}
		return true;
	}
	private Team getTeamById(Integer id) {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM teams WHERE id = " + id);
			if (resultSet.next()) {
				return new Team(resultSet.getInt("id"), resultSet.getString("name"));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return null;
	}
}